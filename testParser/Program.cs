﻿using System;
using simpleParser;

namespace testParser
{
    class Program
    {
        static void Main(string[] args)
        {
            string test = "3/0.7*(1-3)-cos(x)*(1 + 2)";
            Parser parser = new Parser(test);

            Console.WriteLine(parser.Evaluation(0));
        }
    }
}
