﻿using System;
using System.ComponentModel;

namespace RectangleIntegr
{
    public class RectangleInt : Component
    {
        private readonly int _amountDerivatives = 1000;

        public delegate double CalcSecondDerive(double x);
        public delegate double CalcFunction(double x);
        CalcFunction function;
        CalcSecondDerive secondDerive;

        public double calcIntegral(  double a, double b, double eps, 
                                    CalcFunction _function,
                                    CalcSecondDerive _secondDerive)
        {
            Function = _function;
            SecondDerive = _secondDerive;

            double sum = 0;
            double n = calcOptimumNumOfSteps(a, b, eps);
            if (n == 0) //function = const
            {
                return Function(a)*(b - a);
            }
            double step = (b - a) / n;

            Console.WriteLine(step);

            for (double x = a; x <= b - step; x += step )
            {
                // denote (a + b)/2 as (x + x + h)/2 = x + h/2
                sum += Function(x + step / 2);
            }

            return step*sum;
        }

        double calcOptimumNumOfSteps(double a, double b, double eps)
        {
            double maxDerive = getMaxDerive(a, b, eps);
            return Math.Sqrt( Math.Pow((b - a), 3) * Math.Abs (maxDerive) / ( 24 * eps ) );
        }

        double getMaxDerive( double a, double b, double eps)
        {
            double maxDerive = SecondDerive(a);
            if (double.IsInfinity(maxDerive))
            {
                maxDerive = SecondDerive(a + eps * eps);
            }

            double h = (b - a) / _amountDerivatives;

            for (double x = a + h; x <= b; x += h)
            {
                double tempDerive = Math.Abs(SecondDerive(x));
                if (maxDerive < tempDerive && !double.IsInfinity(tempDerive))
                {
                    maxDerive = tempDerive;
                }
            }

            return maxDerive;
        }

        public CalcFunction Function
        {
            get
            {
                if (function == null)
                {
                    throw new Exception("Не задана функция для расчёта выражения");
                }
                return function;
            }
            set 
            {
                function = value;
            }
        }
        public CalcSecondDerive SecondDerive
        {
            get
            {
                if (function == null)
                {
                    throw new Exception("Не задана функция для расчёта производной");
                }
                return secondDerive;
            }
            set
            {
                secondDerive = value;
            }
        }
    }
}
