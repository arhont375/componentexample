﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace simpleParser
{
    public class Parser : Component
    {
        public enum LexemType { sin, cos, ln, plus, minus, mult, div, value, open, close, var };

        string expression;

        List<Term> result;

        public Parser()
        {
        }

        public Parser( string _expression )
        {
            ParseString(_expression);
        }

        void addLexem(Stack<Term> operations, Term current_op)
        {
            current_op.priority = get_priority(current_op.type);

            while (operations.Count > 0 && operations.Peek().type != LexemType.open &&
                        current_op.priority <= operations.Peek().priority)
            {
                result.Add(operations.Pop());
            }

            operations.Push(current_op);
        }
        void findLexem(ref int pos, Stack<Term> operations)
        {
            Term term = new Term();
            
            switch (expression[pos])
            {
                case ' ': break;
                case '(': term.type = LexemType.open;   addLexem(operations, term);      break;
                case ')': find_open(operations); break;
                case '+': term.type = LexemType.plus;   addLexem(operations, term);      break;
                case '-': term.type = LexemType.minus;  addLexem(operations, term);      break;
                case '*': term.type = LexemType.mult;   addLexem(operations, term);      break;
                case '/': term.type = LexemType.div;    addLexem(operations, term);      break;
                case 's': term.type = LexemType.sin;    addLexem(operations, read_sin(ref pos)); break;
                case 'c': term.type = LexemType.cos;    addLexem(operations, read_cos(ref pos)); break;
                case 'l': term.type = LexemType.ln;     addLexem(operations, read_ln(ref pos)); break;
                case 'x': term.type = LexemType.var;    result.Add(term);           break;
                default:
                    result.Add(read_value(ref pos));
                    break;
            }

            ++pos;
        }

        public void ParseString( string _expression )
        {
            Stack<Term> operations = new Stack<Term>();
            result = new List<Term>();

            int pos = 0;

            expression = _expression;

            if (expression.Length == 0)
            {
                throw new Exception("Empty string");
            }

            try
            {
                while (pos < expression.Length)
                {
                    findLexem(ref pos, operations);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Parse Error");
            }

            while (operations.Count != 0)
            {
                result.Add(operations.Pop());
            }
        }

        public double Evaluation( double x = 0)
        {
            Stack<Term> memory = new Stack<Term>();

            for (int i = 0, lengthResultString = result.Count; i < lengthResultString; ++i )
            {
                if (result[i].type == LexemType.value || result[i].type == LexemType.var)
                {
                    memory.Push(result[i]);
                } else if (result[i].type == LexemType.sin || result[i].type == LexemType.cos
                        || result[i].type == LexemType.ln)
                {
                    Term _term = memory.Pop();
                    if (_term.type == LexemType.var)
                    {
                        _term.value = x;
                    }
                    memory.Push(new Term(LexemType.value, eval_unary_function(result[i].type, _term.value)));
                } else if (result[i].type == LexemType.div || result[i].type == LexemType.minus 
                                || result[i].type == LexemType.mult || result[i].type == LexemType.plus)
                {
                    Term _y = memory.Pop();
                    Term _x = memory.Pop();

                    _x.value = (_x.type == LexemType.var ? x : _x.value);
                    _y.value = (_y.type == LexemType.var ? x : _y.value);

                    memory.Push(new Term(LexemType.value, eval_binary_function(result[i].type, _x.value, _y.value)));
                }
            }

            return memory.Pop().value;
        }

        double eval_unary_function( LexemType type, double x)
        {
            switch (type)
            {
                case LexemType.ln:  return Math.Log(x);
                case LexemType.sin: return Math.Sin(x);
                case LexemType.cos: return Math.Cos(x);
            }

            return 0;
        }
        double eval_binary_function( LexemType type, double x, double y)
        {
            switch (type)
            {
                case LexemType.minus:   return x - y;
                case LexemType.plus:    return x + y;
                case LexemType.mult:    return x * y;
                case LexemType.div:
                    {
                        if (y == 0)
                            throw new DivideByZeroException("Деление на ноль, во время"
                                                                + " вычисления выражения");
                        return x / y;
                    }
            }

            return 0;
        }


        Term read_sin(ref int pos)
        {
            if (expression[++pos] == 'i' && expression[++pos] == 'n')
            {
                return (new Term(LexemType.sin));
            }
            throw new Exception("Parse Error");
        }

        Term read_cos(ref int pos)
        {
            if (expression[++pos] == 'o' && expression[++pos] == 's')
            {
                return new Term(LexemType.cos);
            }
            throw new Exception("Parse Error");
        }

        Term read_ln(ref int pos)
        {
            if (expression[++pos] == 'n')
            {
                return (new Term(LexemType.ln));
            }
            throw new Exception("Parse Error");
        }

        Term read_value(ref int pos)
        {
            int start = pos;
            double value;

            while (pos < expression.Length && Char.IsDigit(expression[pos])) 
                ++pos;
            if (pos == expression.Length)
            {
                value = double.Parse(expression.Substring(start, pos - start));
                --pos;
                return (new Term(LexemType.value, value));
            }
            if (expression[pos] == '.')
            {
                ++pos;
                while (pos < expression.Length && Char.IsDigit(expression[pos]))
                    ++pos;
            }

            if (start == pos)
            {
                throw new Exception("Something wrong with numbers");
            }

            value = double.Parse(expression.Substring(start, pos - start));
            --pos;

            return (new Term(LexemType.value, value));
        }
        void find_open(Stack< Term> operations)
        {
            Term currrent_term = operations.Pop();
            while (operations.Count != 0 && currrent_term.type != LexemType.open)
            {
                result.Add(currrent_term);
                currrent_term = operations.Pop();
            }
            if (currrent_term.type != LexemType.open)
            {
                throw new Exception("Broken brackets");
            }
        }

        int get_priority(LexemType type)
        {
            switch (type)
            {
                case LexemType.plus:    return 0;
                case LexemType.minus:   return 0;
                case LexemType.div:     return 1;
                case LexemType.mult:    return 1;
                case LexemType.ln:      return 2;
                case LexemType.cos:     return 2;
                case LexemType.sin:     return 2;
                case LexemType.open:    return 100;
            }
            throw new Exception("Invalid type");
        }
    }
}
