﻿namespace simpleParser
{
    class Term
    {

        public Term()
        {
        }

        public Term(Parser.LexemType _type, double _value = 0)
        {
            type = _type;
            value = _value;
        }

        public Parser.LexemType type
        {
            get;
            set;
        }
        public double value
        {
            get;
            set;
        }
        public int priority
        {
            get;
            set;
        }
    }
}
