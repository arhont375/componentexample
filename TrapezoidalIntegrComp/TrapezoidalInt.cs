﻿using System;
using System.ComponentModel;

namespace TrapezoidalIntegrComp
{
    public class TrapezoidalInt : Component
    {
        public delegate double CalcSecondDerive(double x);
        public delegate double CalcFunction(double x);
        private const int AmountDerivatives = 1000;

        private CalcFunction function;
        private CalcSecondDerive secondDerive;

        private delegate double GetValueDel(double x, CalcFunction function);
        
        public double calcIntegral(double a, double b, double eps,
                                    CalcFunction _function,
                                    CalcSecondDerive _secondDerive)
        {
            Function = _function;
            SecondDerive = _secondDerive;

            GetValueDel getValue = (double xx, CalcFunction calcFunction) =>
            {
                double ans = calcFunction(xx);
                if (double.IsInfinity(ans))
                {
                    ans = calcFunction(xx + 0.001);
                }
                return ans;
            };

            double n = calcOptimumNumOfSteps(a, b, eps);
            if (n == 0) //function = const
            {
                return Function(a) * (b - a);
            }
            double step = (b - a) / n;

            double sum = getValue(a, Function);

            if  (double.IsInfinity(Function(b))) 
            {
                sum += Function(b - 0.01);
            }
            else
            {
                sum += Function(b);
            }
            sum /= 2;
            for (double x = a + step; x <= b - step; x += step)
            {
                sum += getValue(x, Function);
            }

            return step * sum;
        }

        double calcOptimumNumOfSteps(double a, double b, double eps)
        {
            double maxDerive = getMaxDerive(a, b, eps);
            return Math.Sqrt(Math.Pow((b - a), 3) * Math.Abs(maxDerive) / (12 * eps));
        }

        double getMaxDerive(double a, double b, double eps)
        {
            double maxDerive = SecondDerive(a);
            if (double.IsInfinity(maxDerive))
            {
                maxDerive = SecondDerive(a + eps*eps);
            }

            double h = (b - a) / AmountDerivatives;

            for (double x = a + h; x <= b; x += h)
            {
                double tempDerive = Math.Abs(SecondDerive(x));
                if (maxDerive < tempDerive && !double.IsInfinity(tempDerive))
                {
                    maxDerive = tempDerive;
                }
            }

            return maxDerive;
        }
        public CalcFunction Function
        {
            get
            {
                if (function == null)
                {
                    throw new Exception("Не задана функция для расчёта выражения");
                }
                return function;
            }
            set
            {
                function = value;
            }
        }
        public CalcSecondDerive SecondDerive
        {
            get
            {
                if (function == null)
                {
                    throw new Exception("Не задана функция для расчёта производной");
                }
                return secondDerive;
            }
            set
            {
                secondDerive = value;
            }
        }
    }
}
