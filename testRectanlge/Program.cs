﻿using System;
using DerivativeComp;
using RectangleIntegr;
using SimpsonIntregrComp;
using TrapezoidalIntegrComp;

namespace testRectanlge
{
    class Program
    {
        static void Main(string[] args)
        {
            RectangleInt rect = new RectangleInt();
            TrapezoidalInt trapez = new TrapezoidalInt();
            SimpsonInt simps = new SimpsonInt();

            SecondDerive secDerive = new SecondDerive();
            FourthDerive fourthDerive = new FourthDerive();

            secDerive.Calc = function;
            fourthDerive.Calc = function;

            Console.WriteLine( rect.calcIntegral(0, Math.PI, 0.001, function, secDerive.calcSecondDerive ));
            Console.WriteLine(trapez.calcIntegral(0, Math.PI, 0.001, function, secDerive.calcSecondDerive));
            Console.WriteLine(simps.calcIntegral(0, Math.PI, 0.001, function, fourthDerive.calcFourthDerive));
        }

        static double function(double x)
        {
            return Math.Log(1 + Math.Cos(x));
        }
    }
}
