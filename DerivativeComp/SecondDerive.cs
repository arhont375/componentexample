﻿using System;
using System.ComponentModel;

namespace DerivativeComp
{
    public class SecondDerive : Component
    {
        public delegate double calcFunction(double x);

        public double calcSecondDerive(double x)
        {
            if (Calc == null)
            {
                throw new Exception("Не задана функция расчёта");
            }
            double h = 0.0001;
            //double x1 = -Calc(x + 2 * h);
            //double x2 = 16 * Calc(x + h);
            //double x3 = -30 * Calc(x);
            //double x4 = 16 * Calc(x - h);
            //double x5 = -Calc(x - 2 * h);

            return  (-Calc(x + 2*h) + 16*Calc(x + h) - 30*Calc(x) + 16*Calc(x - h) - Calc(x - 2*h)) / (12*h*h);
        }

        public calcFunction Calc
        { private get; set; }
    }
}
