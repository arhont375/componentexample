﻿using System;
using System.ComponentModel;

namespace SimpsonIntregrComp
{
    public class SimpsonInt : Component
    {
        public delegate double CalcFourthDerive(double x);
        public delegate double CalcFunction(double x);
        private const int AmountDerivatives = 10000;

        private CalcFunction function;
        private CalcFourthDerive fourthDerive;

        private delegate double GetValueDel(double x, CalcFunction function);

        public double calcIntegral(double a, double b, double eps,
                                    CalcFunction _function,
                                    CalcFourthDerive _fourthDerive)
        {
            Function = _function;
            FourthDerive = _fourthDerive;

            GetValueDel getValue = (double xx, CalcFunction calcFunction) =>
            {
                double ans = calcFunction(xx);
                if (double.IsInfinity(ans))
                {
                    ans = calcFunction(xx + 0.001);
                }
                return ans;
            };

            double n = calcOptimumNumOfSteps(a, b, eps);
            if (n == 0) //function = const
            {
                return (b - a) * Function(a);
            }
            if (n > 1e7)
            {
                n = 1e7;
            }
            double step = (b - a) / n;

            double sum = getValue(a, Function);
            if  (double.IsInfinity(Function(b))) 
            {
                sum += Function(b - 0.01);
            }
            else
            {
                sum += Function(b);
            }
            double sum_a = 0, sum_b = 0;
            double halfStep = step / 2;

            for (double x = a + step; x <= b - step; x += step)
            {
                sum_a += getValue(x, Function);
                sum_b += getValue(x + halfStep, Function);
            }

            return halfStep * (sum + 2 * sum_a + 4 * sum_b) / 3;
        }

        double calcOptimumNumOfSteps(double a, double b, double eps)
        {
            double maxDerive = getMaxDerive(a, b, eps);
            return Math.Sqrt(Math.Sqrt(Math.Pow((b - a), 5) * Math.Abs(maxDerive) / (2880 * eps)));
        }

        double getMaxDerive(double a, double b, double eps)
        {
            double maxDerive = FourthDerive(a);
            if (double.IsInfinity(maxDerive))
            {
                maxDerive = FourthDerive(a + eps * eps);
            }

            double h = (b - a) / AmountDerivatives;

            for (double x = a + h; x <= b; x += h)
            {
                double tempDerive = Math.Abs(FourthDerive(x));
                if (maxDerive < tempDerive && !double.IsInfinity(tempDerive))
                {
                    maxDerive = tempDerive;
                }
            }

            return maxDerive;
        }
        public CalcFunction Function
        {
            get
            {
                if (function == null)
                {
                    throw new Exception("Не задана функция для расчёта выражения");
                }
                return function;
            }
            set
            {
                function = value;
            }
        }
        public CalcFourthDerive FourthDerive
        {
            get
            {
                if (function == null)
                {
                    throw new Exception("Не задана функция для расчёта производной");
                }
                return fourthDerive;
            }
            set
            {
                fourthDerive = value;
            }
        }
    }
}
