﻿using System;
using System.Collections.Generic;
using System.Windows;
using simpleParser;

namespace IntegralClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    //TODO: распараллелить циклы в интегралах
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonCalc_Click(object sender, RoutedEventArgs e)
        {
            Parser parser;

            try 
	        {
                parser = new Parser(tbExpression.Text);
	        }
	        catch (Exception)
	        {
                tbConsole.Text += "Ошибка в выражении\n";
                return;
	        }

            double a, b, eps;
            if ( ! (double.TryParse( tbA.Text, out a) && 
                 double.TryParse( tbB.Text, out b) && 
                 double.TryParse( tbE.Text, out eps )))
            {
                tbConsole.Text += "Ошибка в задании констант\n";
                return;
            }

            callIntegrMethods(parser, a, b, eps);

            parser.Dispose();
        }

        private void callIntegrMethods(Parser parser,
                                        double a, 
                                        double b,
                                        double eps)
        {
            double? res = null;
            if (buttonRect.IsChecked == true)
            {
                res = runRectMethod(parser, a, b, eps);
            } else if (buttonTrapez.IsChecked == true)
            {
                res = runTrapezMethod(parser, a, b, eps);
            } else if (buttonSimps.IsChecked == true)
            {
                res = runSimpsMethod(parser, a, b, eps);
            } else if (buttonAll.IsChecked == true)
            {
                List<double> ans = runAll(parser, a, b, eps);
                foreach (double x in ans)
                {
                    tbConsole.Text += "Res = " + x + "\n";
                }
                return;
            }

            if (res == null)
            {
                return;
            }
            tbConsole.Text += "Res = " + res + "\n";
        }
    }
}
