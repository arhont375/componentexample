﻿using System.Collections.Generic;
using DerivativeComp;
using RectangleIntegr;
using simpleParser;
using SimpsonIntregrComp;
using TrapezoidalIntegrComp;

namespace IntegralClient
{
    public partial class MainWindow
    {
        double runRectMethod(Parser parser, double a, double b, double eps)
        {
            SecondDerive secDerive = new SecondDerive();
            RectangleInt rect = new RectangleInt();

            secDerive.Calc = parser.Evaluation;
            double res = rect.calcIntegral(a, b, eps, parser.Evaluation, secDerive.calcSecondDerive);

            secDerive.Dispose();
            rect.Dispose();

            return res;
        }
        double runTrapezMethod(Parser parser, double a, double b, double eps)
        {
            SecondDerive secDerive = new SecondDerive();
            TrapezoidalInt trap = new TrapezoidalInt();

            secDerive.Calc = parser.Evaluation;

            double res = trap.calcIntegral(a, b, eps, parser.Evaluation, secDerive.calcSecondDerive);

            secDerive.Dispose();
            trap.Dispose();

            return res;
        }
        double runSimpsMethod(Parser parser, double a, double b, double eps)
        {
            FourthDerive fourthDerive = new FourthDerive();
            SimpsonInt simps = new SimpsonInt();

            fourthDerive.Calc = parser.Evaluation;

            double res = simps.calcIntegral(a, b, eps, parser.Evaluation, fourthDerive.calcFourthDerive);

            fourthDerive.Dispose();
            simps.Dispose();

            return res;
        }

        List< double> runAll(Parser parser, double a, double b, double eps)
        {
            AllMethods.AllMethods container = new AllMethods.AllMethods();
            SecondDerive secDerive = new SecondDerive();
            FourthDerive fourthDerive = new FourthDerive();

            secDerive.Calc = parser.Evaluation;
            fourthDerive.Calc = parser.Evaluation;

            List<double> ans = new List<double>();

            ans.Add(((AllMethods.RectangleInt)container.Schemes.Components["Rectangular"]).
                    CalcIntegral(a, b, eps, parser.Evaluation, secDerive.calcSecondDerive));
            ans.Add(((AllMethods.TrapezoidalInt)container.Schemes.Components["Trapezoidal"]).
                    CalcIntegral(a, b, eps, parser.Evaluation, secDerive.calcSecondDerive));
            ans.Add(((AllMethods.SimpsonInt)container.Schemes.Components["Simpson"]).
                    CalcIntegral(a, b, eps, parser.Evaluation, fourthDerive.calcFourthDerive));


            secDerive.Dispose();
            fourthDerive.Dispose();
            container.Dispose();

            return ans;
        }
            
    }
}
