﻿using System;
using System.ComponentModel;

namespace DerivativeComp
{
    public class FourthDerive : Component
    {
        public delegate double calcFunction(double x);

        public double calcFourthDerive(double x)
        {
            if (Calc == null)
            {
                throw new Exception("Не задана функция расчёта");
            }
            double h = 0.0001;
            //double x1 = -Calc(x + 2 * h);
            //double x2 = 16 * Calc(x + h);
            //double x3 = -30 * Calc(x);
            //double x4 = 16 * Calc(x - h);
            //double x5 = -Calc(x - 2 * h);

            return  (-Calc(x + 3*h) + 12*Calc(x + 2*h) - 39*Calc(x + h) + 
                        56*Calc(x) - 39*Calc(x - h) + 12*Calc(x - 2*h) - Calc( x - 3*h)) / (6*h*h*h*h);
        }

        public calcFunction Calc
        { private get; set; }
    }
}
